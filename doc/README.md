# Omnibus packages for GitLab Runner

- [Installation instructions](install/README.md)
- [Build instructions](build/README.md)

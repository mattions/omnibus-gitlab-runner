# Installing GitLab Runner

All available packages are listed on the [downloads](downloads.md) page.

## Ubuntu 14.04 64-bit

```bash
# Download the package
wget https://s3-eu-west-1.amazonaws.com/downloads-packages/ubuntu-14.04/gitlab-runner_5.2.0~omnibus.1-1_amd64.deb

# Install the package
sudo dpkg -i gitlab-runner_5.2.0~omnibus.1-1_amd64.deb

# Create the user that will run your builds
sudo useradd -s /bin/false -m -r gitlab-runner

# Register your runner instance with a GitLab CI Coordinator
sudo /opt/gitlab-runner/bin/setup -C /home/gitlab-runner

# Install the gitlab-runner Upstart script
sudo cp /opt/gitlab-runner/doc/install/upstart/gitlab-runner.conf /etc/init/

# Start the gitlab-runner Upstart script
sudo service gitlab-runner start
```

Setup is done! You should see the builds starting in GitLab CI coordinator.

You can troubleshoot failing builds by checking the build log.
You can experiment by logging in as gitlab-runner with: `sudo -su gitlab-runner`.

The runner will write log messages via Upstart to
`/var/log/upstart/gitlab-runner.log`.

If you want to test a ruby application, for example GitLab CE, you can use the following to setup runner with ruby:

```bash
# as root
(
set -e
apt-get update
apt-get upgrade -y
apt-get install -y curl
cd /root
rm -rf cookbooks cookbook-gitlab-test.git
curl 'https://gitlab.com/gitlab-org/cookbook-gitlab-test/repository/archive.tar.gz?ref=master' | tar -xvz
mkdir cookbooks
mv cookbook-gitlab-test.git cookbooks/cookbook-gitlab-test
curl -L https://www.chef.io/chef/install.sh | bash
chef-client -z -r 'recipe[cookbook-gitlab-test]'
)

### Register your runner instance with a GitLab CI Coordinator
sudo /opt/gitlab-runner/bin/setup -C /home/gitlab-runner

# Restart the gitlab-runner Upstart script
sudo service gitlab-runner restart
```

## Ubuntu 12.04

Follow the directions under Ubuntu 14.04 but use the package for Ubuntu 12.04.
You can find the package on the [downloads](downloads.md) page.

## Centos 6 64-bit

MR welcome. This would need an alternative Upstart config file (because the one
currently in the repository is not compatible with the Upstart version in
Centos 6).

## Centos 7 64-bit

MR welcome. This would need a SystemD unit file.
[Example](https://gitlab.com/gitlab-org/omnibus-gitlab/blob/1d08929f1f77e719fb360e6f443048ae141880e8/files/gitlab-cookbooks/runit/files/default/gitlab-runsvdir.service).

## Debian 7 64-bit

MR welcome. This would need an init script or maybe `apt-get install runit`
plus a few Runit scripts (`run`, `log/run`).

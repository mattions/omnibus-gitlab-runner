#
# Copyright 2015 GitLab B.V.
#
# All Rights Reserved.
#

name "gitlab-runner"
maintainer "GitLab B.V."
homepage "https://about.gitlab.com"

# Defaults to C:/gitlab-runner on Windows
# and /opt/gitlab-runner on all other platforms
install_dir "#{default_root}/#{name}"

build_version Omnibus::BuildVersion.semver
build_iteration 1

override :ruby, version: '2.1.5'

# Creates required build directories
dependency "preparation"

# gitlab-runner dependencies/components
dependency "gitlab-runner"
dependency "documentation"

# Version manifest file
dependency "version-manifest"

exclude "**/.git"
exclude "**/bundler/git"
